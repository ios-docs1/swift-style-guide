## Credits

[Yerassyl Yerlanov](https://github.com/erlanov14) currently maintains this style guide.
It is a collaborative effort from the most stylish s10s.co team members and its community: 

* [Alpamys Duimagambetov](https://github.com/alpa)
